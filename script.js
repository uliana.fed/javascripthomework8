const paragraphs = document.querySelectorAll("p");
paragraphs.forEach(paragraph => {
  paragraph.style.backgroundColor = "#ff0000";
});

const optionsList = document.getElementById("optionsList");
console.log(optionsList);

const parentElement = optionsList.parentNode;
console.log(parentElement);

const childNodes = optionsList.childNodes
childNodes.forEach(node => {
    console.log("Name node:", node.nodeName);
    console.log("Type node:", node.nodeType);
  });

const testParagraph = document.getElementById("testParagraph");
testParagraph.textContent = "This is a paragraph";

const mainHeader = document.querySelector(".main-header");
const nestedElements = mainHeader.querySelectorAll("*");
nestedElements.forEach(element => {
  console.log(element);
  element.classList.add("nav-item");
});

const sectionTitles = document.querySelectorAll(".section-title");
sectionTitles.forEach( title => {
    title.classList.remove("section-title")
})
